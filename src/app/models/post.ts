export class Post {
    public id: number;
    public title: string;
    public date: string;
    public content: string;

    constructor(title: string, content: string) {
        this.id = Math.floor((Math.random() * 100) + 1)
        this.date = Date.now().toString();
        this.title = title;
        this.content = content;
    } 
}
import { TestBed } from '@angular/core/testing';

import { PostDataService } from './post-data.service';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from 'src/app/app-routing.module';
import { EditorComponent } from 'src/app/editor/editor.component';
import { PostComponent } from 'src/app/post/post.component';
import { HomeComponent } from 'src/app/home/home.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorDirective, FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

describe('PostDataService', () => {
  let service: PostDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({ 
      imports: [RouterTestingModule.withRoutes(routes), ReactiveFormsModule, FroalaEditorModule, FroalaViewModule],
      declarations: [HomeComponent, EditorComponent, PostComponent,],
      providers: [PostDataService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA] 
    });
    service = TestBed.get(PostDataService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // TODO -- Fix service test to test the service....
  it('should return posts data', () => {
  });

});

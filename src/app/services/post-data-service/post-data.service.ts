import { Injectable } from '@angular/core';
import { Post } from '@models/post';
import { Observable, of } from 'rxjs';
import * as data from '../../../assets/posts.json';
import {ActivatedRouteSnapshot, RouterStateSnapshot, Resolve, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class PostDataService implements Resolve<Post> {
  
  postDisplayLimit = 10;
  
  
  constructor(private router: Router) { }
  
  resolve(route: ActivatedRouteSnapshot , state: RouterStateSnapshot): Observable<Post> {
    let id = route.paramMap.get('id');
    let post = this.getPost(parseInt(id));
    console.log('checking post id:' + id);
    if (post) {
      return post;
    } else {
      this.router.navigate(['/home']);
    }
  }
  
  getPosts(): Observable<Post[]> {
    let posts: Post[] = [];
    data.posts.forEach(p => {
      posts.push(p);
    });
    return of(posts);
  }

  getPost(id: number): Observable<Post> {
    let post = data.posts.find(p => p.id = id);
    return of(post);
  }

  putPost(post: Post) {
    let foundIndex = data.posts.findIndex(p => p.id == post.id);
    console.log(foundIndex)
    if (foundIndex != -1) {
      console.log("Orginal");
      console.warn(data.posts[foundIndex]);
      console.log("Replace");
      console.log(post)
      data.posts[foundIndex] = post;
    } else {
      data.posts.push(post);
    }
    // convert json object to string form and write?
    console.log(data);
  }
}

import { TestBed, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { routes } from './app-routing.module'

import { AppModule } from './app.module';


describe('AppRoutingModule', () => {
    let location: Location;
    let router: Router;
    let fixture;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes), AppModule
            ],
            declarations: [
            ],
            providers: [
                Location
            ]
        }).compileComponents();

        router = TestBed.get(Router);
        location = TestBed.get(Location);
        router.initialNavigation();
    });

    it("fakeAsync works", fakeAsync(() => {
        let promise = new Promise(resolve => {
            setTimeout(resolve, 10);
        });
        let done = false;
        promise.then(() => (done = true));
        tick(50);
        expect(done).toBeTruthy();
    }));

    it('site should initially route to "/home"', fakeAsync(() => {
        router.navigate([""]).then(() => {
            expect(location.path()).toBe("/home");
        });
    }));

})
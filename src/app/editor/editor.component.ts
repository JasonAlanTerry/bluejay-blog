import { Component, OnInit } from '@angular/core';
import { Post } from '@models/post';
import { PostDataService } from '@services/post-data-service/post-data.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// -- Post may not be submitted without title.
// -- Highlight title bar, if content exists, but title does not.
// -- When submit clicked, if valid inputs, ask if user is sure.
// -- when submit clicked, if invalid inputs, prompt user to correct

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {


  post: Post;

  editorForm = new FormGroup({
    postTitle: new FormControl(''),
    postContent: new FormControl('')
  })


  editorOptions: any;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private postDataService: PostDataService) {
  }

  ngOnInit() {
    this.getPost();
    this.editorOptions = {
      placeholderText: this.post.content,
      toolbarInline: true,
      charCounterCount: false,
      tabSpaces: 4
    };
  }

  getPost(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));
    console.log(this.route.snapshot.paramMap)
    console.log(id)
    if (id) {
      console.log('existing')
      this.postDataService.getPost(id).subscribe(post => this.post = post);
    } else {
      console.log('new')
      this.post = new Post('Title', 'Content');
    }
  }

  onSubmit() {
    console.warn(this.editorForm.value);
    this.post.title = this.editorForm.value.postTitle;
    this.post.content = this.editorForm.value.postContent;
    this.post.date = Date.now.toString();
    this.postDataService.putPost(this.post);
    this.location.back();
  }

}

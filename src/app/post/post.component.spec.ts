import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostComponent } from './post.component';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../app-routing.module';
import { HomeComponent } from '../home/home.component';
import { EditorComponent } from '../editor/editor.component';
import { PostPreviewComponent } from '../post-preview/post-preview.component';
import { ReactiveFormsModule } from '@angular/forms';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        ReactiveFormsModule, 
        FroalaEditorModule,
        FroalaViewModule
    ],
      declarations: [
        PostComponent, 
        HomeComponent, 
        EditorComponent, 
        PostPreviewComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PostDataService } from '@services/post-data-service/post-data.service';
import { Post } from '@models/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {

  post: Post;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private postDataService: PostDataService
  ) { }

  ngOnInit() {
    this.getPost();
  }

  getPost(): void {
    try {
      const id = parseInt(this.route.snapshot.paramMap.get('id'));
      this.postDataService.getPost(id).subscribe(post => this.post = post);
    } catch(e) {
      console.error(e)
    }
  }

  goBack(): void {
    this.location.back();
  }

}

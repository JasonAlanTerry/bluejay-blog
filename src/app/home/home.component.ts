import { Component, OnInit } from '@angular/core';
import { PostDataService } from '@services/post-data-service/post-data.service';
import { Post } from '../models/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  posts: Post[];

  constructor(private postDataService: PostDataService) { }

  ngOnInit() {
    this.getPosts();

  }

  getPosts(): void {
    this.postDataService.getPosts().subscribe(posts => this.posts = posts);
  }

}
